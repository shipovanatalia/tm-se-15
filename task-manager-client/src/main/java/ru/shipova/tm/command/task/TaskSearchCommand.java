package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

import java.util.List;

public class TaskSearchCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-search";
    }

    @Override
    public String getDescription() {
        return "Search task by part of name.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getSessionDTO();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(sessionDTO, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK SEARCH]");
                @NotNull final String partOfData = serviceLocator.getTerminalService().nextLine();
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                @Nullable final List<TaskDTO> taskList;
                taskList = taskEndpoint.searchTask(sessionDTO, partOfData);
                if (taskList == null || taskList.isEmpty()) {
                    System.out.println("TASKS ARE NOT FOUND");
                    return;
                }
                int index = 1;
                for (@NotNull final TaskDTO task : taskList) {
                    System.out.println(index++ + ". " + task.getName());
                }
                System.out.println();
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
