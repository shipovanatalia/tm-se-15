package ru.shipova.tm;

import ru.shipova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ClientApplication {
    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(ClientApplication.class).initialize()
                .select(Bootstrap.class).get().init();
    }
}
