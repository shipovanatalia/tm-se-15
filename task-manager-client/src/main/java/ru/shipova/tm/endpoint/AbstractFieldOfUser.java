
package ru.shipova.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for abstractFieldOfUser complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="abstractFieldOfUser"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://endpoint.tm.shipova.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateOfBegin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateOfCreate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="dateOfEnd" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://endpoint.tm.shipova.ru/}status" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractFieldOfUser", propOrder = {
    "dateOfBegin",
    "dateOfCreate",
    "dateOfEnd",
    "status"
})
@XmlSeeAlso({
    Project.class,
    Task.class
})
public class AbstractFieldOfUser
    extends AbstractEntity
{

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBegin;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfCreate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfEnd;
    @XmlSchemaType(name = "string")
    protected Status status;

    /**
     * Gets the value of the dateOfBegin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBegin() {
        return dateOfBegin;
    }

    /**
     * Sets the value of the dateOfBegin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBegin(XMLGregorianCalendar value) {
        this.dateOfBegin = value;
    }

    /**
     * Gets the value of the dateOfCreate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfCreate() {
        return dateOfCreate;
    }

    /**
     * Sets the value of the dateOfCreate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfCreate(XMLGregorianCalendar value) {
        this.dateOfCreate = value;
    }

    /**
     * Gets the value of the dateOfEnd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfEnd() {
        return dateOfEnd;
    }

    /**
     * Sets the value of the dateOfEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfEnd(XMLGregorianCalendar value) {
        this.dateOfEnd = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link Status }
     *     
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link Status }
     *     
     */
    public void setStatus(Status value) {
        this.status = value;
    }

}
