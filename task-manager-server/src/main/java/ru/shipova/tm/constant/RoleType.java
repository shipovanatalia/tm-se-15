package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum RoleType {
    ADMIN("ADMIN"),
    USER("USER");

    private @NotNull final String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }

    @Nullable
    public static RoleType getRoleType(@Nullable final String displayName){
        for (@NotNull final RoleType roleType: RoleType.values()) {
            if (roleType.displayName().equals(displayName))
                return roleType;
        }
        return null;
    }
}
