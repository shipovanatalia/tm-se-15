package ru.shipova.tm.util;

import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class DateToSqlDateUtil {
    public static LocalDateTime formatDate(@Nullable final Date date) throws ParseException {
        //SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        if (date == null) return null;
        return date.toInstant().atZone(ZoneId.of("Europe/Moscow")).toLocalDateTime();
    }
}
