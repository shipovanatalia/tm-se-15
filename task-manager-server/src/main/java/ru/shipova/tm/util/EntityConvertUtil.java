package ru.shipova.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.dto.UserDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class EntityConvertUtil {

    @Nullable
    public static UserDTO userToUserDTO(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }

    @Nullable
    public static User userDTOToUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) return null;
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRoleType(userDTO.getRoleType());
        return user;
    }

    @NotNull
    public static List<User> userDTOListToUserList(@NotNull final List<UserDTO> userDTOList) {
        @NotNull final List<User> userList = new ArrayList<>();
        for (UserDTO userDTO : userDTOList) {
            userList.add(userDTOToUser(userDTO));
        }
        return userList;
    }

    @NotNull
    public static List<UserDTO> userListToUserDTOList(@NotNull final List<User> userList) {
        @NotNull final List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : userList) {
            userDTOList.add(userToUserDTO(user));
        }
        return userDTOList;
    }

    @Nullable
    public static ProjectDTO projectToProjectDTO(@Nullable final Project project){
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateOfCreate(project.getDateOfCreate());
        projectDTO.setDateOfBegin(project.getDateOfBegin());
        projectDTO.setDateOfEnd(project.getDateOfEnd());
        return projectDTO;
    }

    @Nullable
    public static Project projectDTOToProject(@Nullable final ProjectDTO projectDTO){
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        @NotNull final User user = new User();
        user.setId(projectDTO.getUserId());
        project.setUser(user);
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateOfCreate(projectDTO.getDateOfCreate());
        project.setDateOfBegin(projectDTO.getDateOfBegin());
        project.setDateOfEnd(projectDTO.getDateOfEnd());
        return project;
    }

    @NotNull
    public static List<Project> projectDTOListToProjectList(@Nullable final List<ProjectDTO> projectDTOList) {
        if (projectDTOList == null) return new ArrayList<>();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : projectDTOList) {
            projectList.add(projectDTOToProject(projectDTO));
        }
        return projectList;
    }

    @NotNull
    public static List<ProjectDTO> projectListToProjectDTOList(@NotNull final List<Project> projectList) {
        @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projectList) {
            projectDTOList.add(projectToProjectDTO(project));
        }
        return projectDTOList;
    }

    @Nullable
    public static TaskDTO taskToTaskDTO(@Nullable final Task task){
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateOfCreate(task.getDateOfCreate());
        taskDTO.setDateOfBegin(task.getDateOfBegin());
        taskDTO.setDateOfEnd(task.getDateOfEnd());
        return taskDTO;
    }

    @Nullable
    public static Task taskDTOToTask(@Nullable final TaskDTO taskDTO){
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        @NotNull final Project project = new Project();
        @NotNull final User user = new User();
        project.setId(taskDTO.getProjectId());
        user.setId(taskDTO.getUserId());
        task.setUser(user);
        task.setProject(project);
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateOfCreate(taskDTO.getDateOfCreate());
        task.setDateOfBegin(taskDTO.getDateOfBegin());
        task.setDateOfEnd(taskDTO.getDateOfEnd());
        return task;
    }

    @NotNull
    public static List<Task> taskDTOListToTaskList(@Nullable final List<TaskDTO> taskDTOList) {
        if (taskDTOList == null) return new ArrayList<>();
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (TaskDTO taskDTO : taskDTOList) {
            taskList.add(taskDTOToTask(taskDTO));
        }
        return taskList;
    }

    @NotNull
    public static List<TaskDTO> taskListToTaskDTOList(@NotNull final List<Task> taskList) {
        @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskList) {
            taskDTOList.add(taskToTaskDTO(task));
        }
        return taskDTOList;
    }

    @NotNull
    public static SessionDTO sessionToSessionDTO(@NotNull final Session session){
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setId(session.getId());
        return sessionDTO;
    }

    @NotNull
    public static Session sessionDTOToSession(@NotNull final SessionDTO sessionDTO){
        @NotNull final Session session = new Session();
        @NotNull final User user = new User();
        user.setId(sessionDTO.getUserId());
        session.setUser(user);
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setId(sessionDTO.getId());
        return session;
    }
}
