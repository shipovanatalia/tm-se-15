package ru.shipova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.DomainDTO;

import java.io.IOException;

public interface ISerializer {
    void serialize(@NotNull final DomainDTO domainDTO) throws IOException;
    @Nullable DomainDTO deserialize() throws IOException;
}