package ru.shipova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.AccessForbiddenException;


public interface ISessionEndpoint {
    @Nullable
    SessionDTO openSession(@NotNull final User user);
    @Nullable
    Result closeSession(@NotNull final SessionDTO sessionDTO) throws AccessForbiddenException;
}
