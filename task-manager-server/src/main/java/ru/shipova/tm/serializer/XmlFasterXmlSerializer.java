package ru.shipova.tm.serializer;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.dto.DomainDTO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator.Feature.WRITE_XML_1_1;
import static java.nio.charset.StandardCharsets.UTF_8;
import static ru.shipova.tm.constant.DataConstant.FILE_XML;

public class XmlFasterXmlSerializer implements ISerializer {
    @NotNull
    private final XmlMapper xmlMapper = new XmlMapper();

    @Override
    public void serialize(@NotNull final DomainDTO domainDTO) throws IOException {
        xmlMapper.configure(WRITE_XML_1_1, true);
        @NotNull final ObjectWriter objectWriter = xmlMapper.writerWithDefaultPrettyPrinter();
        @NotNull final String xml = objectWriter.writeValueAsString(domainDTO);
        @NotNull final byte[] data = xml.getBytes(UTF_8);
        @NotNull final File file = new File(FILE_XML.displayName());
        Files.write(file.toPath(), data);
    }

    @Override
    @Nullable
    public DomainDTO deserialize() throws IOException {
        @NotNull final File file = new File(FILE_XML.displayName());
        @Nullable final DomainDTO domainDTO = xmlMapper.readValue(file, DomainDTO.class);
        return domainDTO;
    }
}
