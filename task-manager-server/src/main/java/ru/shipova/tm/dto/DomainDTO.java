package ru.shipova.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class DomainDTO implements Serializable {
    @NotNull private String userId = "";
    @Nullable private List<UserDTO> userList = new ArrayList<>();
    @Nullable private List<ProjectDTO> projectList = new ArrayList<>();
    @Nullable private List<TaskDTO> taskList = new ArrayList<>();
}
