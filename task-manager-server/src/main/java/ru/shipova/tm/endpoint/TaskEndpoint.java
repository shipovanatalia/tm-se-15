package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Singleton
@WebService(name = "TaskEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class TaskEndpoint extends AbstractEndpoint{

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(@Nullable final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    @NotNull
    public List<TaskDTO> showAllTasksOfProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (bootstrap == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        @Nullable final List<Task> taskList = bootstrap.getTaskService().showAllTasksOfProject(projectName);
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> getTaskListOfUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (bootstrap == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        @Nullable final List<Task> taskList = bootstrap.getTaskService().getTaskListOfUser(session.getUser().getId());
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    @Nullable
    public List<TaskDTO> searchTask(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "partOfData", partName = "partOfData") final String partOfData
    ) throws AccessForbiddenException {
        if (bootstrap == null) return new ArrayList<>();
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        @Nullable final List<Task> taskList = bootstrap.getTaskService().search(session.getUser().getId(), partOfData);
        return EntityConvertUtil.taskListToTaskDTOList(taskList);
    }

    @WebMethod
    public void createTask(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    ) throws AccessForbiddenException, ProjectDoesNotExistException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        bootstrap.getTaskService().create(session.getUser().getId(), taskName, projectName);
    }

    @WebMethod
    public void clearTaskListOfUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO
    ) throws AccessForbiddenException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        bootstrap.getTaskService().clear(session.getUser().getId());
    }

    @WebMethod
    public void removeTaskFromUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @Nullable final String taskName
    ) throws AccessForbiddenException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        bootstrap.getTaskService().remove(session.getUser().getId(), taskName);
    }

    @WebMethod
    public void loadTaskList(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "taskDTOList", partName = "taskDTOList") @Nullable final List<TaskDTO> taskDTOList
    ) throws AccessForbiddenException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        List<Task> taskList = EntityConvertUtil.taskDTOListToTaskList(taskDTOList);
        bootstrap.getSessionService().validate(session);
        bootstrap.getTaskService().load(taskList);
    }
}
