package ru.shipova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.bootstrap.Bootstrap;

import javax.inject.Singleton;

@Singleton
@NoArgsConstructor
public class AbstractEndpoint {
    @Nullable
    public Bootstrap bootstrap;

    public AbstractEndpoint(@Nullable final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }
}
