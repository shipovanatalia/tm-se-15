package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Singleton
@WebService(name = "AdminUserEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class AdminUserEndpoint extends AbstractEndpoint{
    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@Nullable final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    public boolean haveAccessToUsualCommand(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "needAuthorize", partName = "needAuthorize") final boolean needAuthorize
    ) throws AccessForbiddenException {
        if (bootstrap == null) return false;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getAdminUserService().getAccessToUsualCommand(session, needAuthorize);
    }

    @WebMethod
    public boolean haveAccessToAdminCommand(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "isOnlyAdminCommand", partName = "isOnlyAdminCommand") final boolean isOnlyAdminCommand
    ) throws AccessForbiddenException {
        if (bootstrap == null) return false;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        return bootstrap.getAdminUserService().getAccessToAdminCommand(session, isOnlyAdminCommand);
    }
}
