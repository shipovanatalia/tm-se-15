package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.dto.DomainDTO;
import ru.shipova.tm.dto.SessionDTO;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;

@Singleton
@WebService(name = "DomainEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class DomainEndpoint extends AbstractEndpoint {
    public DomainEndpoint() {
        super(null);
    }

    public DomainEndpoint(@Nullable Bootstrap bootstrap) {
        super(bootstrap);
    }

    @WebMethod
    public void serialize(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String serializer
    ) throws AccessForbiddenException, IOException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        @NotNull final DomainDTO domainDTO = new DomainDTO();
        domainDTO.setUserId(session.getUser().getId());
        bootstrap.getDomainService().export(domainDTO);
        bootstrap.getDomainService().serialize(session, domainDTO, serializer);
    }

    @WebMethod
    public void deserialize(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull final SessionDTO sessionDTO,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String deserializer
    ) throws AccessForbiddenException {
        if (bootstrap == null) return;
        @NotNull final Session session = EntityConvertUtil.sessionDTOToSession(sessionDTO);
        bootstrap.getSessionService().validate(session);
        bootstrap.getDomainService().deserialize(session, deserializer);
    }
}
