package ru.shipova.tm.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.api.service.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.service.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.List;

/**
 * Класс загрузчика приложения
 */

@Getter
@Setter
@Singleton
public final class Bootstrap {
    @Inject
    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Inject
    @NotNull
    private final IProjectService projectService = new ProjectService();

    @Inject
    @NotNull
    private final ITaskService taskService = new TaskService();

    @Inject
    @NotNull
    private final IUserService userService = new UserService();

    @Inject
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Inject
    @NotNull
    private final ISessionService sessionService = new SessionService();

    @Inject
    @NotNull
    private final IAdminUserService adminUserService = new AdminUserService(userService);

    @Inject
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @Inject
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @Inject
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint();

    @Inject
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint();

    @Inject
    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpoint();

    @Inject
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint();

    public void init() {
        initEndpoint();
        @Nullable final List<User> userList;
        userList = userService.getUserList();
        if (userList == null || userList.isEmpty()) initDefaultUsers();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(projectEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(adminUserEndpoint);
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(wsdl, endpoint);
    }

    private void initDefaultUsers() {
        try {
            userService.createUser("user", "user", RoleType.USER);
            userService.createUser("admin", "admin", RoleType.ADMIN);
        } catch (LoginAlreadyExistsException e) {
            e.printStackTrace();
        }
    }
}
