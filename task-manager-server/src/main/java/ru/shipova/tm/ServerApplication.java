package ru.shipova.tm;

import ru.shipova.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;

public final class ServerApplication {
    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(ServerApplication.class).initialize()
                .select(Bootstrap.class).get().init();
    }
}
