package ru.shipova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

@Getter
@Singleton
public class PropertyService {
    @NotNull private final String serverHost = "localhost";
    @NotNull private final Integer serverPort = 8180;
}
