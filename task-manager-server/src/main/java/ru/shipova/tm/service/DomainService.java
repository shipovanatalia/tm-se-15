package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.api.service.IDomainService;
import ru.shipova.tm.bootstrap.Bootstrap;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.dto.DomainDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.serializer.*;
import ru.shipova.tm.util.EntityConvertUtil;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class DomainService implements IDomainService {

    private final Bootstrap bootstrap;

    public DomainService(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void deserialize(@NotNull final Session session, @NotNull final String serializer) {
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            @Nullable DomainDTO domainDTO;
            try {
                domainDTO = binarySerializer.deserialize();
                load(domainDTO);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            @Nullable DomainDTO domainDTO;
            try {
                domainDTO = jsonFasterXmlSerializer.deserialize();
                load(domainDTO);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            @Nullable DomainDTO domainDTO;
            try {
                domainDTO = jsonJaxBSerializer.deserialize();
                load(domainDTO);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            @Nullable DomainDTO domainDTO;
            try {
                domainDTO = xmlFasterXmlSerializer.deserialize();
                load(domainDTO);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            @Nullable DomainDTO domainDTO;
            try {
                domainDTO = xmlJaxBSerializer.deserialize();
                load(domainDTO);
            } catch (IOException e) {
                System.out.println("OOPS, YOU HAVE A PROBLEM WITH DESERIALIZATION.");
            }
        }
    }

    @Override
    public void load(@Nullable final DomainDTO domainDTO) {
        if (domainDTO == null) return;
        bootstrap.getUserService().load(EntityConvertUtil.userDTOListToUserList(domainDTO.getUserList()));
        bootstrap.getProjectService().load(EntityConvertUtil.projectDTOListToProjectList(domainDTO.getProjectList()));
        bootstrap.getTaskService().load(EntityConvertUtil.taskDTOListToTaskList(domainDTO.getTaskList()));
    }

    @Override
    public void serialize(@NotNull final Session session, @NotNull final DomainDTO domainDTO, @NotNull String serializer) throws IOException {
        domainDTO.setUserId(session.getUser().getId());
        export(domainDTO);
        if (Serializer.BINARY.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer binarySerializer = new BinarySerializer();
            binarySerializer.serialize(domainDTO);
        }
        if (Serializer.JSON_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonFasterXmlSerializer = new JsonFasterXmlSerializer();
            jsonFasterXmlSerializer.serialize(domainDTO);
        }
        if (Serializer.JSON_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer jsonJaxBSerializer = new JsonJaxBSerializer();
            jsonJaxBSerializer.serialize(domainDTO);
        }
        if (Serializer.XML_FASTER_XML.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlFasterXmlSerializer = new XmlFasterXmlSerializer();
            xmlFasterXmlSerializer.serialize(domainDTO);
        }
        if (Serializer.XML_JAX_B.displayName().equals(serializer.toUpperCase())) {
            @NotNull final ISerializer xmlJaxBSerializer = new XmlJaxBSerializer();
            xmlJaxBSerializer.serialize(domainDTO);
        }
    }

    @Override
    public void export(@Nullable final DomainDTO domainDTO) {
        if (domainDTO == null) return;
        @Nullable final List<User> userList = bootstrap.getUserService().getUserList();
        @Nullable final List<Project> projectList = new ArrayList<>();
        @Nullable final List<Task> taskList = new ArrayList<>();
        if (userList == null) return;
        for (@Nullable final User user : userList) {
            if (user == null) return;
            @Nullable final List<Project> projectListOfUser =
                    bootstrap.getProjectService().getProjectListOfUser(user.getId());
            @Nullable final List<Task> taskListOfUser
                    = bootstrap.getTaskService().getTaskListOfUser(user.getId());
            if (projectListOfUser == null) return;
            if (taskListOfUser == null) return;
            projectList.addAll(projectListOfUser);
            taskList.addAll(taskListOfUser);
        }
        domainDTO.setUserList(EntityConvertUtil.userListToUserDTOList(userList));
        domainDTO.setProjectList(EntityConvertUtil.projectListToProjectDTOList(projectList));
        domainDTO.setTaskList(EntityConvertUtil.taskListToTaskDTOList(taskList));
    }
}
